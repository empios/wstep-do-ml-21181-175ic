import os
from urllib import request
from imgurpython import ImgurClient
import timeit

import threading
from concurrent.futures import ThreadPoolExecutor

import asyncio
import aiohttp

client_secret = "b87675d8a613cb32488a352d916e652b1ed4bcc6"
client_id = "124fef33252b504"

client = ImgurClient(client_id, client_secret)



def main():
    choise = 1
    if(choise == 1):
        counter = 0
        images = client.gallery()
        for image in images:
            counter+=1
            request.urlretrieve(image.link, "pobrane/{}.{}".format(counter, 'gif'))
            print("{}.{} pobrano do folderu".format(counter, 'gif'))
            if(counter == 10): 
                break
        choise +=1
    if(choise == 2):
        def download_image():
            filename1 = str(counter)+"_multi"
            request.urlretrieve(image.link, "pobrane/{}.{}".format(filename1, 'gif'))
            print("{}.{} pobrano do folderu".format(filename1, 'gif'))
        images = client.gallery()
        with ThreadPoolExecutor(max_workers=5) as executor:
            executor.map(download_image, images)
        choise +=1
    if(choise == 3):
        filename2 = str(counter)+"_isync"
        images = client.gallery()
        async def download_images(link, session):
            async with session.get(link) as response:
                with open("pobrane/{}.{}".format(filename2, 'gif'), 'wb') as fd:
                    async for data in response.content.iter_chunked(1024):
                        fd.write(data)
            async with aiohttp.ClientSession() as session:
                tasks = [download_images(image.link, session) for image in images]
            return await asyncio.gather(*tasks)
    return choise

if __name__ == "__main__":
    print("Czas potrzebny na pobranie sposobem synchronicznym: {}".format(timeit.Timer(main).timeit(number=1)))
    print("Czas potrzebny na pobranie przez multithreading: {}".format(timeit.Timer(main).timeit(number=1)))
    start_time = timeit.default_timer()
    time_taken = timeit.default_timer() - start_time
    print("Czas potrzebny na pobranie przez AsyncIO: {}".format(time_taken))