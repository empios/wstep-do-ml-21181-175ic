import imutils
import cv2

image = cv2.imread("bmw.jpg")
(h, w, d) = image.shape
print("width={}, height={}, depth={}".format(w, h, d))
(B, G, R) = image[100, 50]
print("R={}, G={}, B={}".format(R, G, B))
wycinka = image[1:100, 120:250]
resized = cv2.resize(image, (420, 200))
center = (w // 2, h // 2)
M = cv2.getRotationMatrix2D(center, -60, 1.0)
rotated = cv2.warpAffine(image, M, (w, h))
blurred = cv2.GaussianBlur(image, (9, 9), 0)
output = image.copy()
output2 = image.copy()
cv2.rectangle(output, (301, 279), (357, 210), (0, 0, 255), 2)
text = cv2.putText(output2, "Wnetrze mojego BMW", (10, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)
cv2.imshow("Naniesiony tekst", text)
cv2.imshow("Zarys znaczka BMW", output)
cv2.imshow("Zamazane", blurred)
cv2.imshow("Zmiana parametrow", resized)
cv2.imshow("Obrazek", image)
cv2.imshow("Wycinka", wycinka)
cv2.imshow("Obrot", rotated)
cv2.waitKey(0)