import pandas as pd
from pandasgui import show

csv = pd.read_csv('samochody1tys.csv')



print('1.Pobranie listy kolumn')
kolumny = csv.columns.tolist()
print(kolumny)

print('2.Zmiana nazwy kolumny')
noweKolumny = csv.rename(columns={'marka': 'Marka Samochodu'})
print(noweKolumny.columns.tolist())

print('3.Częstotliwość zdarzeń - rok produkcji auta')
print(csv['rok_produkcji'].value_counts())

print('4.Sortowanie po cenie od najwiekszej')
print(csv.sort_values(by=['cena'], ascending=False))

print('5.Mnożenie ceny przez np. Podatek')
print(csv['cena'].apply(lambda x: x * 1.23))

print('6.Usuwanie wartoności pustych - jeśli nie ma podanej ceny')
print(csv.dropna(axis=1,inplace=False))

print('7.Sprawdzenie typów kolumn')
print(csv.dtypes)

print('8.Pobranie pierwszych i ostatnich wynikow')
print(csv.head())
print(csv.tail())

print('9.Pobranie kluczowych informacji o naszych danych')
print(csv.describe())

print('10.Zastąpienie pustych komórek daną wartością')
print(csv.fillna(value="hej"))

print('11. Otworzenie GUI dla Pandas')
gui = show(csv,settings={'block': True})