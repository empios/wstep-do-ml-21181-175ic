
print("1. Wczytanie pliku w trybie - odczytu")
reader = open('pawel_wlodarczyk_21181.txt', 'r')
try:
    print("2. Odczyt pliku")
    print(reader.read())
    line = reader.readline()
    print("3. Odczyt po końcu linii")
    while line != '':
        print(line, end='')
        line=reader.readline()
    print("4. Dopis słowa do pliku")
    with open('pawel_wlodarczyk_21181_reverse.txt', 'w') as writer:
         writer.write("Naura")
finally:
    reader.close()

print("5. Odczyt obrazka")
with open('bmw.jpg', 'rb') as byte_reader:
    print(byte_reader.read(1))
    print(byte_reader.read(3))
    print(byte_reader.read(2))
    print(byte_reader.read(1))
    print(byte_reader.read(1))
