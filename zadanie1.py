import numpy as np

arr10 = np.zeros((10,), dtype=int)
print('ZAD 1')
print(arr10)

arr5 = np.full(10, 5)
print('ZAD 2')
print(arr5)

arr10_50 = [*range(10, 51, 1)]
print('ZAD 3')
print(arr10_50)

randmatrix3x3 = np.random.randint(0, 9, [3, 3])
createMatrix = np.mat(randmatrix3x3)
print('ZAD 4')
print(createMatrix)


matrixIden = np.identity(3)
print('ZAD 5')
print(matrixIden)

mat5x5 = np.random.normal(0,10,size=(5,5))
print('ZAD 6')
print(mat5x5)

mat10x10 = np.random.choice(np.arange(0.01, 1, 0.1), (10, 10))
mat10x10 = np.mat(mat10x10)
print('ZAD 7')
print(mat10x10)

matrixlinear = np.linspace(0,1,20)
print('ZAD 8')
print(matrixlinear)

print('ZAD 9')
lista = np.random.randint(1, 26, 25)
lista = lista.reshape(5, 5)
lista = np.mat(lista)
print(lista)


print('A)')
print(lista.sum())

print('B)')
print(lista.mean())

print('C)')
print(lista.std())

print('D)')
column_sums = lista.sum(axis=0)
print(column_sums)

print('ZAD 10')
matrix100 = np.random.randint(0, 101, [5, 5])
matrix100 = np.mat(matrix100)
print('A)')
print(matrix100)
print('B)')
print(np.median(matrix100))
print('C)')
print(matrix100.min())
print('D)')
print(matrix100.max())


print('ZAD 11')
trans_matrix100 = np.random.randint(0, 101, [4, 3])
trans_matrix100 = np.mat(trans_matrix100)
print(trans_matrix100.transpose())


print('ZAD 12')
a = np.random.randint(5, 11, [3, 3], dtype=int)
a = np.mat(a)
b = np.random.randint(5, 11, [3, 3], dtype=int)
b = np.mat(b)
result = np.zeros((3, 3), dtype=int)
result = np.mat(result)

for i in range(len(a)):
    for j in range(len(a[0])):
        result[i][j] = a[i][j] + b[i][j]
print(result)

print('ZAD 13')
x = np.random.randint(5, 11, [3, 2], dtype=int)
x = np.mat(x)
y = np.random.randint(5, 11, [2, 3], dtype=int)
y = np.mat(y)
print(np.matmul(x, y))
print('\n dot:')
print(np.dot(x,y))