import json
from urllib.request import urlopen
import csv
import pandas

jsonurltodos = urlopen("https://jsonplaceholder.typicode.com/todos")
todos = json.load(jsonurltodos)
todos_by_user = {}
for todo in todos:
    if todo["completed"]:
        try:
            todos_by_user[todo["userId"]] += 1
        except KeyError:
            todos_by_user[todo["userId"]] = 1
top_users = sorted(todos_by_user.items(), key=lambda x: x[1], reverse=True)
max_complete = top_users[0][1]
users = []
for user, num_complete in top_users:
    if num_complete < max_complete:
        break
    users.append(str(user))
max_users = " i ".join(users)
s = "cy" if len(users) > 1 else ""
print(f"Użytkowni{s} {max_users} ukończyli {max_complete} TODO")

userName = []
for user in users:
    jsonurlusers = urlopen("https://jsonplaceholder.typicode.com/users/" + user)
    userjson = json.load(jsonurlusers)
    userName.append(userjson["name"])

printuser = " i ".join(userName)
print(f"Użytkownicy nazywają się {printuser}")


jsonurlalbums= urlopen("https://jsonplaceholder.typicode.com/albums")
albumjson = json.load(jsonurlalbums)

data_file = open('album.csv', 'w') 
csv_writer = csv.writer(data_file) 
count = 0
for album in albumjson: 
    if count == 0: 
        header = album.keys() 
        csv_writer.writerow(header) 
        count += 1
    csv_writer.writerow(album.values()) 
data_file.close() 

df = pandas.read_csv('album.csv')

df2 = pandas.read_csv('album.csv', index_col='id', names=['id','title','userId'])