import spacy
from collections import Counter
from spacy import displacy
nlp = spacy.load("pl_core_news_sm")

print("1. Czytanie tekstu")
text = "Witaj na nowym pliku laboratoryjnym."
text_arr = nlp(text)
print([token.text for token in text_arr])

print("2. Rozpoznawanie zdań")
about_text = ("Cześć!" "Oto mój tekst" " ze zdaniami." "Cieszę się"  ".")
about_doc = nlp(about_text)
sentences = list(about_doc.sents)
for sentence in sentences:
    print (sentence)

print("3. Słowa 'stop' w polskim języku")
stopwords = spacy.lang.pl.stop_words.STOP_WORDS
print("ilość: ", len(stopwords))
print("Przykłady:")
for stop_word in list(stopwords)[:10]:
    print(stop_word)


print("4. Lematyzacja -> sprowadzenie słów do ich wyrazów podstawowych.")
lem_text = ("To jest przykładowy"
            " tekst do pomocy"
            " przy Lematyzacji w celach naukowych.")
lem_text_doc = nlp(lem_text)
for token in lem_text_doc:
    print(token, token.lemma_)

print("5. Zliczanie częstotliwości słów w podanym tekscie.")
freq_text = "Litwo! Ojczyzno moja! Ty jesteś jak zdrowie. Ile cię stracił." \
            " Dziś piękność zda się zatrudniał i przyjaciel domu). " \
            "Widząc gościa, na awanpostach nasz z nim i przeplatane różowymi wstęgi pośród nich wzory zmieniano wiarę, mowę, prawa i jak krzykną: ura! - niewola!" \
            " Pamiętam, chociaż w domu lasami i całował. Zaczęła się uparta coraz głośniejsza kłótnia o ten tylko się trzeba, i Obuchowicz Piotrowski, Obolewski, Rożycki, Janowicz, Mirzejewscy, Brochocki i stajennym i długie zwijały" \
            " się cukier wytapia i zwycięzca, wydartych potomkom Cezarów rzucił wzrok surowy znać człowieka rodu, obyczajów! Dość, że go bronią od płaczącej matki pod las i " \
            "długie paznokcie przedstawiając dwa smycze chartów przedziwnie udawał psy głupie a wzdycha do sieni siadł przy pełnym kielich nalać i jak bazyliszek. asesor mniej był żonaty a" \
            " chłopi żegnali się, jak mógł schwytać. Wojskiego Woźny ciągle jako swe rodzinne duszę jego upadkiem domy i z parkanu na trzykrólskie święta przesuwają w charta. Tak każe przyzwoitość)." \
            " nikt lepiej zna równie pędzel, noty, druki. Aż osłupiał Tadeusz Telimenie, Asesor zaś dowodził na Francuza. oj, ten tylko widział krótki, jasnozłoty a ja wam służyć, moje."

freq_text_doc = nlp(freq_text)
words = [token.text for token in freq_text_doc
    if not token.is_stop and not token.is_punct]
word_freq = Counter(words)
common_words = word_freq.most_common(5)
unique_words = [word for (word, freq) in word_freq.items() if freq == 1]
print ("Najczęstsze słowa: ", common_words)
print ("Unikalne słowa: ", unique_words)

print("6. Sprawdzanie roli danego słowa w zdaniu.")
for token in about_doc:
    print (token, token.tag_, token.pos_, spacy.explain(token.tag_))

#WIZUALIZACJA ZDANIA
visual_text = "Michał opisywał interesujące góry z widokiem na morze."
visual_text_doc = nlp(visual_text)
displacy.serve(visual_text_doc, style="dep")

print("8. Sprawdzenie gramatycznie jak powstało zdanie.")
piano_text = "Patryk gra w piłkę nożną."
piano_doc = nlp(piano_text)
for token in piano_doc:
    print (token.text, token.tag_, token.head.text, token.dep_)

print("9. Wizualizacja tekstu gramatycznie")
displacy.serve(piano_doc, style='dep')

print("10. Kategoryzowanie poszczególnych składników zdania")
cat_text="Muzeum Powstania Warszawskiego znajduję się blisko centrum miasta."
cat_text_doc = nlp(cat_text)
for ent in cat_text_doc.ents:
    print(ent.text, ent.start_char, ent.end_char,
        ent.label_, spacy.explain(ent.label_))
print("11. Wizualizacja kategorii zdania")
displacy.serve(cat_text_doc, style='ent')