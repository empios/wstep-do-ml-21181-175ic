LAB 9

image = cv2.imread("bmw.jpg") -> tutaj oczytujemy nasz obrazek

(h, w, d) = image.shape -> tutaj odczytujemy paramtery naszego obrazka jak: h -> height, w-> width

te metody pozwalają nam pokazanie obrazka bez jego zamykania automatycznego (dopiero CTRL + Z zamyka proces):
cv2.imshow("Image", image)
cv2.waitKey(0)

(B, G, R) = image[100, 50] -> tutaj odczytujemy wartość RGB dla danego piksela

wycinka = image[38:100, 233:500] -> takim sposobem ucinania pikseli w tablicy możemy przyciąć nasz obazek do wymiarów jakie chcemy

resized = cv2.resize(image, (200, 200)) -> tym sposobem możemy zmienić parametry naszego obrazka (wysokość i szerokość)

M = cv2.getRotationMatrix2D(center, -60, 1.0) -> ustawiamy rotację w przestrzeni 2D
rotated = cv2.warpAffine(image, M, (w, h)) -> używamy rotacji na naszym obrazku

blurred = cv2.GaussianBlur(image, (9, 9), 0) -> tutaj uzywamy metody do rozmazania Gaussa naszego obrazka aby wyglądał "jak przez mgłę"

cv2.rectangle(output, (301, 279), (357, 210), (0, 0, 255), 2) -> tutaj rysujemy prostokąt do naszego obrazka gdzie pierwsza to pozycja startowa nastepnie piksele konczace (X, Y)

text = cv2.putText(output2, "Wnetrze mojego BMW", (10, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2) -> ta metoda pozwala nam dodanie tekstu do naszego obrazka w wybranym kolorze oraz foncie i określonym miejscu