import numpy as np
import pandas as pd
from tabulate import tabulate


def funkcja_aktywacji(inputData):
	inputData = float(inputData)
	if inputData < 0:
		return 0
	else:
		return 1



def oblicz_warstwy_sieci(inputData,weigh,func):
	inputData = list(inputData)
	weigh = list(weigh)
	inputData = np.array([1]+inputData).transpose()
	weigh = np.array(weigh).transpose()
	output = inputData.dot(weigh)
	if type(output) != np.ndarray:
		output = [output]
	else:
		output.T
	if func:
		output = [(funkcja_aktywacji(x),x) for x in output]
	return output


def wyswietlWynik(inputData, weigh, bias,output, gateName):
	wynik = []
	for idx,input in enumerate(inputData):
		Act,noAct = output[idx]
		wynik.append([input,weigh,bias,noAct,Act])
	print("~~~~~~" + gateName + "~~~~~~")
	for len in wynik:
		print("--------------------------------------------------------------------------------")
		print("Wejście")
		print(len[0])
		print("Wagi")
		print(len[1])
		print("Bias")
		print(len[2])
		print("Wartość bez funkcji")
		print(len[3])
		print("Z funkcją")
		print(len[4])
		print("--------------------------------------------------------------------------------")
	


weigh = [1,1]
inputData = [[0, 0], [0, 1], [1, 0], [1, 1]]
bias = -1
output = []
for idx,inputData in enumerate(inputData):
	out,raw_out = oblicz_warstwy_sieci(inputData,[bias]+weigh,oblicz_warstwy_sieci)[0]
	output.append((out,raw_out))
wyswietlWynik(inputData, weigh, bias, output, "AND")

###########OR

weigh = [1,1]
inputData = [[0, 0], [0, 1], [1, 0], [1, 1]]
bias = 0.5
output = []
for input in inputData:
	out,raw_out = oblicz_warstwy_sieci(input,[bias]+weigh,oblicz_warstwy_sieci)[0]
	output.append((out,raw_out))
wyswietlWynik(inputData, weigh, bias, output, "OR")


###########NOT

weigh = [-2]
inputData = [[1], [0]]
bias = 3
output = []
for input in inputData:
	out,raw_out = oblicz_warstwy_sieci(input,[bias]+weigh,oblicz_warstwy_sieci)[0]
	output.append((out,raw_out))
wyswietlWynik(inputData, weigh, bias, output, "NOT")


###########XNOR

weigh = [[-1,-1],[1,1]]
inputData = [[1, 1], [1, 0], [0, 1], [0, 0]]
bias = [[1.5],[-1.5]]
output = []
for input in inputData:
	results = oblicz_warstwy_sieci(input,np.concatenate((bias,weigh),axis=1),oblicz_warstwy_sieci)
	output.append(([result[0]for result in results],[result[1]for result in results]))
wyswietlWynik(inputData, weigh, bias, output, "XNOR")

###########XOR

weigh = [[1,1],[-1,-1]]
inputData = [[1, 1], [1, 0], [0, 1], [0, 0]]
bias = [[-1],[1.5]]
outputs = []
for input in inputData:
	results = oblicz_warstwy_sieci(input,np.concatenate((bias,weigh),axis=1),oblicz_warstwy_sieci)
	outputs.append(([result[0]for result in results],[result[1]for result in results]))
wyswietlWynik(inputData, weigh, bias, output, "XOR")