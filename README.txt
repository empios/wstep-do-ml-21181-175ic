reader = open('pawel_wlodarczyk_21181.txt', 'r') -> metoda daje możliwość odczytu pliku tylko i wyłącznie gdzie pierwsze
jest plikiem który odczytujemy a dalej mamy tryb.

reader.read() -> Czyta zawartość całego pliku

line = reader.readline() -> odczyt całej linii tekstu.

Metoda poniżej przedstawia czytanie kolejnej linii jeśli nie skończy się tekst.
 while line != '':
        print(line, end='')
        line=reader.readline()

Otworzenie danego pliku w tybie edycji i dopianie słowa do pliku.
    with open('pawel_wlodarczyk_21181_reverse.txt', 'w') as writer:
         writer.write("Naura")

Zamknięcie pliku
reader.close()

Metoda poniżej otwiera nam plik jpg i czyta poszczególne bajty.
with open('bmw.jpg', 'rb') as byte_reader:
    print(byte_reader.read(1))
    print(byte_reader.read(3))
    print(byte_reader.read(2))
    print(byte_reader.read(1))
    print(byte_reader.read(1))

Możemy również tym samym sposobem otwierać np. pliki PDF czy chociażby pliki wav z dźwiękiem.



####################################################################################################


LAB 8

urlopen("https://jsonplaceholder.typicode.com/todos") -> metoda pozwala nam otworzyć link na którym w tym przypadku znajduje się nasz json.

json.load(jsonurltodos) -> tutaj parsujemy jsona przez bibliotekę.

Dalej w kodzie używamy pętli for do sprawdzenia ukończonych zadań dla poszczególnych użytkowników.

Wypisuje potem id użytkowników którzy ukończyli najwięcej zadań oraz poniżej używam połączenia z uzytkownikami aby wypisać imiona i nazwiska tych uzytkowników.


Drugą częścią laboratorium jest przetworzenie pliku CSV wykorzystam do tego JSONA którego przerobię na plik CSV

Po przerobieniu JSON'a zapisałem plik album.csv.

Parametr przy czytaniu CSV przez pandas index_col - > zamienia nam pierwszą kolumnę na wybraną przez nas w tym przypadku wybrałem kolumnę "id" ponieważ wczesniej była "userId"

Parametr names -> daje nam możliwość zmiany nazwy dla poszczególnych nazw kolumn.

####################################################################################################

